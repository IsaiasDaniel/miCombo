import React from 'react'

class ItemCombo extends React.Component{
  handlerOver(){
    this.props.onSelect(this.props.index, true)
  }

  handlerClick(){
    this.props.onSelect(this.props.index, false)
  }

  render(){
    var info = React.cloneElement(this.props.children, this.props.data)

    return (<li className={this.props.isSelected ? 'selected': null}
                onMouseOver={this.handlerOver.bind(this)}
                onClick={this.handlerClick.bind(this)}
                id={this.props.id + this.props.index}>
              {info}
            </li>)
  }
}

class OptionsCombo extends React.Component{

  constructor(props) {
    super(props);
  }

  handlerSelect(indexItem, show){
    this.props.onSelect(indexItem, show)
  }

  componentDidUpdate(){
    if (this.props.scroll) {
      var id = this.props.id + this.props.itemSelected,
          element = document.getElementById(this.props.id),
          elementItem = document.getElementById(id);
      element.scrollTop = elementItem.clientHeight * this.props.itemSelected;
    }
  }

  render(){
    var classAttr = 'optionsCombo ' + (this.props.show ? 'show' : 'hidden'),
        items = this.props.data.map(
          function(data, index){
            return (<ItemCombo key={index}
                           id={this.props.id}
                           index={index}
                           onSelect={this.handlerSelect.bind(this)}
                           isSelected={this.props.itemSelected === index}
                           data={data}>
                  {this.props.children}
                </ItemCombo>)
          }, this);

    return (
      <div id={this.props.id}
           name={this.props.name}
           className={classAttr}>
        <ul>
          {items}
        </ul>
      </div>
    )
  }
}

export default OptionsCombo
