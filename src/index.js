import 'core-js/fn/object/assign';
require('./styles/General.scss')
import React from 'react';
import ReactDOM from 'react-dom';
import MiCombo from './components/MiCombo';

ReactDOM.render(<MiCombo />, document.getElementById('app'));

export default MiCombo
