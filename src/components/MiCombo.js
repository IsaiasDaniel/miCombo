require('normalize.css/normalize.css')
require('./miCombo.scss')

import React from 'react'
import OptionsCombo from './OptionsCombo'

class GenericItem extends React.Component{
  render(){
    return (<div className="item">
              <div className="img" >
                <img src={this.props.img}/>
              </div>
              <div className="info">
                <div className="title">{this.props.title}</div>
                <div className="subtitle">{this.props.subtitle}</div>
              </div>
            </div>)
  }
}

GenericItem.defaultProps = {
  img: 'http://loremflickr.com/30/30',
  title: 'Isaias Daniel',
  subtitle: 'email'
}

class MiCombo extends React.Component {

  constructor(props){
    super(props)
    this.state = {index: -1,
                  value: undefined,
                  showList: false }
  }

  handlerKey(event){
    var state = this.state,
        maxElement = this.props.list.length-1
    state.scroll = true
    if (event.keyCode === 27) {
      state.showList = false
      state.index = -1
    } else  if (event.keyCode === 38) {
      event.preventDefault()
      state.index -= state.index > 0 ? 1 : 0
      state.showList = true
    }
    else if (event.keyCode === 40){
      event.preventDefault()
      state.index += state.index < maxElement ? 1: 0
      state.showList = true
    }
    this.setState(state)
  }

  handlerSelect(index, showList){
    var state = this.state
    if (!showList) {
      var element = document.getElementById(this.props.id),
          value = this.props.list[index]

      if (this.props.onSelect) {
        this.props.onSelect(value)
      }
      if (this.props.subKeyValue) {
        value = value[this.props.subKeyValue]
      }
      element.value = value[this.props.keyValue]
      element.focus()
    }
    state.index = index
    state.showList = showList
    state.scroll = false
    this.setState(state)
  }

  handlerEnter(event){
    if (event.key === 'Enter') {
      event.preventDefault()
      event.stopPropagation()
      var value = this.props.list[this.state.index],
          state = this.state
      if (this.props.subKeyValue) {
        value = value[this.props.subKeyValue]
      }
      if (this.props.onChange){
        state.index = -1
        this.props.onChange(event)
      }
      state.showList = false
      event.target.value = value[this.props.keyValue]
      event.target.focus()
      this.setState(state)
    }
  }

  handlerChange(event){
    if (this.props.onChange) {
      this.props.onChange(event)
      var state = this.state
      state.showList = true
      this.setState(state)
    }
   }

  handlerFocus(){
    var state = this.state,
        lists = document.getElementsByClassName('optionsCombo show'),
        elementShow = lists ? lists[0] : null
    if (elementShow) {
      elementShow.className = 'optionsCombo hidden'
    }
    state.showList = false
    this.setState(state)
  }

  render() {
    return (
      <div className="MiCombo">
        <input id={this.props.id}
               type="text"
               autoComplete="off"
               name={this.props.name}
               value={this.state.value}
               placeholder={this.props.placeholder}
               onFocus={this.handlerFocus.bind(this)}
               onKeyDown={this.handlerKey.bind(this)}
               onChange={this.handlerChange.bind(this)}
               onKeyPress={this.handlerEnter.bind(this)}/>
        <OptionsCombo data={this.props.list}
                      id={this.props.id+'Options'}
                      name={this.props.name}
                      itemSelected={this.state.index}
                      scroll={this.state.scroll}
                      show={this.state.showList}
                      onSelect={this.handlerSelect.bind(this)}>
          {this.props.children || <GenericItem />}
        </OptionsCombo>
      </div>
    )
  }
}

MiCombo.defaultProps = {
  id: 'miCombo',
  keyValue: 'title',
  name: 'name',
  placeholder: 'MiComboBox',
  showLabel: false,
  showSideButton: false,
  list: [ {title:'Stim',
           img:'https://robohash.org/rationecumquis.jpg?size=40x40',
           subtitle:'lhawkins0@examiner.com'}]
}

export default MiCombo
