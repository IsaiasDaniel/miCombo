# Intento de comboBox

![Combo](https://media.giphy.com/media/arA0nHm4qWD8A/giphy.gif)

## Como usarlo

### Parametros

- *id* identificador sera usado en attr id
- *keyValue* elemento de la estructura de datos que sera usado para value de input
- *name* attr name del input
- *placeholder*
- *list* Lista de datos
  * *Ejemplo* list para dato ItemCusto
```
{name:'Stim',
 avatar:'https://robohash.org/rationecumquis.jpg?size=40x40',
 email:'lhawkins0@examiner.com'}, ...]
```

### Eventos
  * onChange Cada que se presiona alguna tecla sobre el input args ``event``
  * onSelect Al seleccionar un elemento de la lista args value el elemento ``obj`` selecciondado de la lista

### General

```
<div className="example">
  <MiCombo id="otroCombo"
           list={data}
           name="email"
           keyValue='email'
           placeholder="otroCombo">
  </MiCombo>
</div>
```
### Custom Item

```
<div className="example">
  <MiCombo id="customItem"
           list={data2}
           name="first_name"
           keyValue='first_name'
           placeholder="customItem">
         < CustomItem/>
  </MiCombo>
</div>
```

---
## Requerimienitos

- reactjs

## ToDo

  - [ ] seleccion de varias optiones, multiselect
  - [ ] mejorar input (crear propio input mejorado) hacer posible la entra de tags
